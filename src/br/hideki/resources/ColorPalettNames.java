package br.hideki.resources;

public enum ColorPalettNames {
	BLUE_PURPLE, PASTEL_PINK, 
	DARK_PINK, DARK_BLUE, 
	PASTEL_YELLOW, DARK_YELLOW,
	PASTEL_GREEN, DARK_GREEN
}
