package br.hideki.gui;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import br.hideki.resources.ColorPalettNames;

public class Main extends JFrame{
	private JMenuBar menuBar;
	private JMenu menuFile;
	private JMenuItem menuItemSave;
	private JMenuItem menuItemOpen;
	private JMenuItem menuItemFlush;
	private JMenu menuFilter;
	private JMenuItem menuItemFilterScreen;
	private JImagePanel imagePanel;
	private String imagePath;
	private ThemeAbstractFabric themeFabric;
	
	Main() {
		super("** Filty **");
		themeFabric = new ThemeDefaultFabric();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(themeFabric.getColor(ColorPalettNames.BLUE_PURPLE));
		setSize(1000, 700);
		setLocationRelativeTo(null);
		setLayout(new GridLayout(1, 1, 0, 0));
		buildMainScreen();

	}

	private void buildMainScreen() {
		menuBar = themeFabric.makeMenuBar();
		menuFile = themeFabric.makeMenu(" File ");
		menuBar.add(menuFile);
		menuItemOpen = themeFabric.makeMenuItem(" Open");
		menuFile.add(menuItemOpen);
		menuItemSave = themeFabric.makeMenuItem(" Save");
		menuFile.add(menuItemSave);
		menuItemFlush = themeFabric.makeMenuItem(" Flush");
		menuFile.add(menuItemFlush);
		menuFilter = themeFabric.makeMenu(" Filter ");
		menuBar.add(menuFilter);
		menuItemFilterScreen = themeFabric.makeMenuItem(" Filter Screen ");
		menuFilter.add(menuItemFilterScreen);
		
		this.setJMenuBar(menuBar);
		menuFilter.setEnabled(false);
		menuItemFlush.setEnabled(false);
		menuItemSave.setEnabled(false);
		
		menuItemOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadImage();
			}
		});
		
		menuItemOpen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				
			}
		});
		
		menuItemFlush.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				imagePanel.showOriginalImage();
			}
		});
		
		menuItemFilterScreen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SubMainFilters.getInstance().setImagePreview(JImagePanel.getInstance().getDisplayImage());
				SubMainFilters.getInstance().setVisible(true);
			}
		});
		
		menuItemSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName =  JOptionPane.showInputDialog(null, "To save your work\n "
																   + "You'll need a name for your c r e a t i o n \n", 
																     "Save", JOptionPane.INFORMATION_MESSAGE);
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int option = fileChooser.showOpenDialog(null);
				if(option == JFileChooser.APPROVE_OPTION) {
					String folderPath = fileChooser.getSelectedFile().getAbsolutePath();
					imagePanel.saveImageFile(folderPath, fileName);
				}
				
			}
		});
		
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_EQUALS) {
					zoomIn();
				}
				if (e.getKeyCode() == KeyEvent.VK_MINUS) {
					zoomOut();
				}
			}
		});
		this.setVisible(true);
	}
	
	private void loadImage() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Images", "jpg", "png", "jpeg"));
		int option = fileChooser.showOpenDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {			
			imagePath = fileChooser.getSelectedFile().getAbsolutePath();
			imagePanel = JImagePanel.getInstance();
			imagePanel.setImage(imagePath);
			this.add(imagePanel);
			this.setVisible(true);
			
			menuFilter.setEnabled(true);
			menuItemFlush.setEnabled(true);
			menuItemSave.setEnabled(true);
		}
	}

	private void zoomIn() {
		imagePanel.zoomIn();
	}

	private void zoomOut() {
		imagePanel.zoomOut();
	}
	
	public static void main(String[] args) {
		new Main();
	}
}
