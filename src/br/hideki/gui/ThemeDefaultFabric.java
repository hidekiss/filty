package br.hideki.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import br.hideki.filters.Filterable;
import br.hideki.resources.ColorPalettNames;

public class ThemeDefaultFabric extends ThemeAbstractFabric {
	
	private Font fontTitle;
	private Font fontItem;
	private static Vector<Color> colorPallet;
	
	private void fillColorPalett() {
		colorPallet.addElement(new Color(195,195,255));
		colorPallet.addElement(new Color(255,150,160));
		colorPallet.addElement(new Color(230,120,130));
		colorPallet.addElement(new Color(70,70,100));
		colorPallet.addElement(new Color(255,240,150));
		colorPallet.addElement(new Color(210,210,85));
		colorPallet.addElement(new Color(190,235,190));
		colorPallet.addElement(new Color(80,180,80));
	}
	
	private void loadFonts() {
		try {
			File file = new File("src/br/hideki/resources/Shrikhand-Regular.ttf");
			fontTitle = Font.createFont(Font.TRUETYPE_FONT, file);
			fontTitle = fontTitle.deriveFont(Font.PLAIN,14);
			file = new File("src/br/hideki/resources/Hack-BoldItalic.ttf");
			fontItem = Font.createFont(Font.TRUETYPE_FONT, file);
			fontItem = fontItem.deriveFont(Font.PLAIN, 18);
		}catch (IOException | FontFormatException e ) {
			System.out.println("the font couldn't be loaded");
		}
	}
	
	public Color getColor(ColorPalettNames colorName) {
		switch (colorName) {
			case BLUE_PURPLE: return colorPallet.get(0);
			case PASTEL_PINK: return colorPallet.get(1);
			case DARK_PINK: return colorPallet.get(2);
			case DARK_BLUE: return colorPallet.get(3);
			case PASTEL_YELLOW: return colorPallet.get(4);
			case DARK_YELLOW: return colorPallet.get(5);
			case PASTEL_GREEN: return colorPallet.get(6);
			case DARK_GREEN: return colorPallet.get(7);
			default: return null;
		}
	}
	
	public ThemeDefaultFabric() {
		loadFonts();
		
		colorPallet = new Vector<>();
		fillColorPalett();	
	}
	
	@Override
	public JLabel makeLabel(String text) {
		
		JLabel label = new JLabel(text);
		label.setFont(fontTitle);
		label.setBorder(BorderFactory.createEmptyBorder());
		label.setHorizontalTextPosition(JLabel.CENTER);
		return label;
	}

	@Override
	public JButton makeButton(ImageIcon image) {
		JButton button = new JButton();
		button.setIcon(image);
		button.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, true));
		return button;
	}

	@Override
	public JButton makeButton(String text) {
		JButton button = new JButton();
		button.setText(text);
		button.setFont(fontTitle.deriveFont(18f));
		button.setBackground(getColor(ColorPalettNames.PASTEL_YELLOW));
		button.setBorder(BorderFactory.createLineBorder(getColor(ColorPalettNames.DARK_YELLOW), 4, true));
		button.setForeground(new Color(70,70,100));
		return button;
	}

	@Override
	public Vector<Color> getColorPalett() {
		return colorPallet;
	}
	
	@Override
	public void setColorPalett(Vector<Color> colors) {
		colorPallet = colors;
	}

	@Override
	public JMenuBar makeMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorder(BorderFactory.createMatteBorder(0, 0, 12, 0, getColor(ColorPalettNames.DARK_PINK)));
		menuBar.setFont(fontTitle);
		menuBar.setBackground(getColor(ColorPalettNames.PASTEL_PINK));
		return menuBar;
	}

	@Override
	public JMenu makeMenu(String name) {
		JMenu menu = new JMenu(name);
		menu.setFont(fontTitle.deriveFont(Font.BOLD, 24));
		menu.setForeground(getColor(ColorPalettNames.DARK_BLUE));
		menu.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
		return menu;
	}

	@Override
	public JMenuItem makeMenuItem(String name) {
		JMenuItem menuItem = new JMenuItem(name);
		menuItem.setBackground(getColor(ColorPalettNames.PASTEL_GREEN));
		menuItem.setText(name);
		menuItem.setFont(fontTitle.deriveFont(17.5f));
		menuItem.setForeground(getColor(ColorPalettNames.DARK_BLUE));
		menuItem.setBorder(BorderFactory.createMatteBorder(0, 0, 3, 0, getColor(ColorPalettNames.DARK_GREEN)));
		return menuItem;
	}
	
	@Override
	public JList<Filterable> makeList(Vector<Filterable> vector) {
		JList<Filterable> list = new JList<>(vector);
		list.setBorder(BorderFactory.createLineBorder(getColor(ColorPalettNames.DARK_YELLOW),5, false));
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setBackground(getColor(ColorPalettNames.PASTEL_YELLOW));
		list.setFont(fontItem.deriveFont(Font.BOLD, 15));
		list.setForeground(getColor(ColorPalettNames.DARK_BLUE));
		return list;
	}
	
	@Override
	public JScrollPane makeScrollPane(Component component) {
		JScrollPane scrollPane = new JScrollPane(component);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		return scrollPane;
	}
}
