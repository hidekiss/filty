package br.hideki.gui;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

import br.hideki.filters.Filterable;
import br.hideki.resources.ColorPalettNames;

public abstract class ThemeAbstractFabric {
	public abstract Vector<Color> getColorPalett();
	public abstract Color getColor(ColorPalettNames colorName);
	public abstract void setColorPalett(Vector<Color> colors);
	public abstract JLabel makeLabel(String text);
	public abstract JButton makeButton(ImageIcon icon);
	public abstract JButton makeButton(String text);
	public abstract JMenuBar makeMenuBar();
	public abstract JMenu makeMenu(String name);
	public abstract JMenuItem makeMenuItem(String name);
	public abstract JScrollPane makeScrollPane(Component component);
	public abstract JList<Filterable> makeList(Vector<Filterable> list);
	
}
