package br.hideki.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Stack;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import br.hideki.filters.ABSSubtract;
import br.hideki.filters.BlackDots;
import br.hideki.filters.Desaturate;
import br.hideki.filters.Filterable;
import br.hideki.filters.GausianBlur;
import br.hideki.filters.GrayScale;
import br.hideki.filters.HSB;
import br.hideki.filters.HighMin;
import br.hideki.filters.Invert;
import br.hideki.filters.InvertedNoise;
import br.hideki.filters.MaxRGB;
import br.hideki.filters.MinMiddle;
import br.hideki.filters.MinRGB;
import br.hideki.filters.Noise;
import br.hideki.filters.OverSaturate;
import br.hideki.filters.Pink;
import br.hideki.filters.PurpleNoise;
import br.hideki.filters.Sharpen;
import br.hideki.filters.Sharpen1;
import br.hideki.filters.Sharpen2;
import br.hideki.filters.Sharpen3;
import br.hideki.filters.Sharpen4;
import br.hideki.filters.SlowPixelSort;
import br.hideki.filters.SortBGR;
import br.hideki.filters.SortBrightness;
import br.hideki.filters.SortRGB;
import br.hideki.filters.SwapOne;
import br.hideki.resources.ColorPalettNames;

public class SubMainFilters extends JFrame{
	private static SubMainFilters subMainFilters = null;
	private Vector<Filterable> filters;
	private Stack<BufferedImage> operations;
	private BufferedImage originalImagePreview;
	private BufferedImage displayImagePreview;
	private GridBagLayout layout;
	private GridBagConstraints layoutConstraints;
	private JList<Filterable> filtersList;
	private JScrollPane scrollList;
	private JPanel panelList; 
	private JLabel imagePreviewLabel;
	private JButton applyButton;
	private JButton undoButton;
	private JButton clearButton;
	private JButton finishButton;
	private ThemeAbstractFabric themeFabric;
	
	private SubMainFilters() {
		super("Filters Galery");
		originalImagePreview = JImagePanel.copyPixels(JImagePanel.getInstance().getDisplayImage());
		displayImagePreview = JImagePanel.copyPixels(originalImagePreview);
		themeFabric = new ThemeDefaultFabric();
		layout = new GridBagLayout();
		layoutConstraints = new GridBagConstraints();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setResizable(true);
		setSize(600, 500);
		getContentPane().setBackground(themeFabric.getColor(ColorPalettNames.BLUE_PURPLE));
		setLayout(layout);
		BuildScreen();
	}
	
	public static SubMainFilters getInstance() {
		if(subMainFilters == null) 
			subMainFilters = new SubMainFilters();
		return subMainFilters;
	}
	
	private void BuildScreen() {
		imagePreviewLabel = new JLabel(new ImageIcon(originalImagePreview.getScaledInstance(300, 300, Image.SCALE_SMOOTH)));
		imagePreviewLabel.setBorder(BorderFactory.createLineBorder(new Color(210,210,60), 4, true));
		addComponent(imagePreviewLabel, GridBagConstraints.WEST, GridBagConstraints.NONE, 0, 0, 1, 1);
		
		operations = new Stack<>();
		filters = new Vector<>();
		fillVector();
		filtersList = themeFabric.makeList(filters);
		add(themeFabric.makeScrollPane(filtersList));
		scrollList = themeFabric.makeScrollPane(filtersList);
		panelList = new JPanel();
		panelList.add(scrollList);
		panelList.setLayout(new GridLayout(1, 1));
		addComponent(panelList, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, 0, 1, 2, 1);

		applyButton = themeFabric.makeButton("   Apply   ");
		addComponent(applyButton, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, 1, 1, 1, 1);
		
		undoButton = themeFabric.makeButton("Undo ");
		addComponent(undoButton, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 1, 2, 1, 1);
		
		clearButton = themeFabric.makeButton("Clean");
		addComponent(clearButton, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 2, 1, 1, 1);
		
		finishButton = themeFabric.makeButton("  Finish  ");
		addComponent(finishButton, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, 3, 3, 1, 1);
		
		applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] indeces = filtersList.getSelectedIndices();
				for (int index: indeces) {
					operations.push(JImagePanel.copyPixels(displayImagePreview));
					displayImagePreview = filters.get(index).apply(displayImagePreview);
				}
				filtersList.clearSelection();
				imagePreviewLabel.setIcon(new ImageIcon(displayImagePreview.getScaledInstance(300, 300, Image.SCALE_SMOOTH)));
			}
		});
		
		undoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!operations.empty()) {
					 displayImagePreview = operations.pop();
					 imagePreviewLabel.setIcon(new ImageIcon(displayImagePreview.getScaledInstance(300, 300, Image.SCALE_SMOOTH)));
				}
			}
		});
		
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				imagePreviewLabel.setIcon(new ImageIcon(originalImagePreview.getScaledInstance(300, 300, Image.SCALE_SMOOTH)));
				operations.clear();
				filtersList.clearSelection();
			}
		});
		
		finishButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JImagePanel.getInstance().setImage(displayImagePreview);
				operations.clear();
				filtersList.clearSelection();
				dispose();		
			}
		});
	}
	
	
	private void fillVector() {
		filters.addElement(new ABSSubtract());
		filters.addElement(new BlackDots());
		filters.addElement(new Desaturate());
		filters.addElement(new GausianBlur());
		filters.addElement(new GrayScale());
		filters.addElement(new HighMin());
		filters.addElement(new HSB());
		filters.addElement(new Invert());
		filters.addElement(new InvertedNoise());
		filters.addElement(new MaxRGB());
		filters.addElement(new MinMiddle());
		filters.addElement(new MinRGB());
		filters.addElement(new Noise());
		filters.addElement(new OverSaturate());
		filters.addElement(new Pink());
		filters.addElement(new PurpleNoise());
		filters.addElement(new Sharpen());
		filters.addElement(new Sharpen1());
		filters.addElement(new Sharpen2());
		filters.addElement(new Sharpen3());
		filters.addElement(new Sharpen4());
		filters.addElement(new SlowPixelSort());
		filters.addElement(new SortBGR());
		filters.addElement(new SortRGB());
		filters.addElement(new SortBrightness());
		filters.addElement(new SwapOne());
	}
	
	private void addComponent(Component component, int anchor, int fill, int row, int column, int width, int height) {
		layoutConstraints.anchor = anchor;
		layoutConstraints.fill = fill;
		layoutConstraints.gridy = row;
		layoutConstraints.gridx = column;
		layoutConstraints.gridwidth = width;
		layoutConstraints.gridheight = height;
		layoutConstraints.insets = new Insets(2, 2, 2, 2);
		layout.setConstraints(component, layoutConstraints);
		this.add(component);
	}

	public void setImagePreview(BufferedImage image) {
		originalImagePreview = JImagePanel.copyPixels(image);
		displayImagePreview = JImagePanel.copyPixels(originalImagePreview);
		imagePreviewLabel.setIcon(new ImageIcon(displayImagePreview.getScaledInstance(300, 300, Image.SCALE_SMOOTH)));
	}
	
}