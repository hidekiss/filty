package br.hideki.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

public class JImagePanel extends JPanel {
	private static JImagePanel jImagePanel = null;
	private BufferedImage originalImage;
	private BufferedImage displayImage;
	private String imageExtension;
	private JLabel imageLabel;
	private ImageIcon imageIcon;
	private JPopupMenu popUp;
	private JMenuItem popUpItemZoomIn;
	private JMenuItem popUpItemZoomOut;
	private JMenuItem popUpItemFilterScreen;
	private ThemeAbstractFabric themeFabric;

	private JImagePanel() {
		super();
		themeFabric = new ThemeDefaultFabric();
		setLayout(new GridLayout(1, 1, 5, 5));
		imageLabel = new JLabel();
		imageLabel.setHorizontalAlignment(JLabel.CENTER);
		imageLabel.setVerticalAlignment(JLabel.CENTER);
		imageLabel.setOpaque(true);
		imageLabel.setBorder(BorderFactory.createMatteBorder(7, 7, 7, 7, new Color(230,120,130)));
		imageLabel.setBackground(new Color(225,160,180));
		add(themeFabric.makeScrollPane(imageLabel));
		buildPopUp();
	}

	private void buildPopUp() {
		popUp = new JPopupMenu("Edit");
		popUpItemZoomIn = themeFabric.makeMenuItem("Zoom In");
		popUp.add(popUpItemZoomIn);
		popUpItemZoomOut = themeFabric.makeMenuItem("Zoom Out");
		popUp.add(popUpItemZoomOut);
		popUpItemFilterScreen = themeFabric.makeMenuItem("Filter Screen");
		popUp.add(popUpItemFilterScreen);
		popUp.setBackground(Color.BLACK);
		imageLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				popUp.show(e.getComponent(), e.getX(), e.getY());
			}
		});
		
		popUpItemZoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				zoomIn();
			}
		});
		
		popUpItemZoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				zoomOut();
			}
		});
		
		popUpItemFilterScreen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SubMainFilters.getInstance().setImagePreview(JImagePanel.getInstance().getDisplayImage());
				SubMainFilters.getInstance().setVisible(true);
			}
		});
	}
	
	public static JImagePanel getInstance() {
		if (jImagePanel == null)
			jImagePanel = new JImagePanel();
		return jImagePanel;
	}

	public void showOriginalImage() {
		imageLabel.setIcon(new ImageIcon(originalImage));
		displayImage = copyPixels(originalImage);
	}

	public void zoomIn() {
		AffineTransform resize = new AffineTransform();
		resize.scale(1.11, 1.11);
		AffineTransformOp resizeOp = new AffineTransformOp(resize, AffineTransformOp.TYPE_BICUBIC);
		displayImage = resizeOp.filter(displayImage, null);
		imageLabel.setIcon(new ImageIcon(displayImage));
	}

	public void zoomOut() {
		AffineTransform resize = new AffineTransform();
		resize.scale(0.9, 0.9);
		AffineTransformOp resizeOp = new AffineTransformOp(resize, AffineTransformOp.TYPE_BICUBIC);
		displayImage = resizeOp.filter(displayImage, null);
		imageLabel.setIcon(new ImageIcon(displayImage));
	}

	public BufferedImage getDisplayImage() {
		return displayImage;
	}

	public BufferedImage getOriginalImage() {
		return originalImage;
	}

	public static BufferedImage copyPixels(BufferedImage image) {
		BufferedImage copy = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				copy.setRGB(i, j, color.getRGB());
			}
		}
		return copy;
	}
	
	public void setImage(BufferedImage image) {
		displayImage = copyPixels(image);
		imageLabel.setIcon(new ImageIcon(displayImage));
	}
	
	public void setImage(String imagePath) {
		try {
			imageExtension = imagePath.substring(imagePath.lastIndexOf(".")+1);
			originalImage = ImageIO.read(new File(imagePath));
			displayImage = copyPixels(originalImage);
			imageIcon = new ImageIcon(originalImage);
			imageLabel.setIcon(imageIcon);
		} catch (IOException e) {
			System.out.println("Ops, the file couldn't be open");
		}
	}

	public void saveImageFile(String imageDirectory, String imageFileName) {
		try {
			ImageIO.write(displayImage, imageExtension, new File(imageDirectory +"/"+ imageFileName + "." + imageExtension));
		}catch (IOException e) {
			System.out.println("Ops, the file couldn't be saved");
		}
	}
}
