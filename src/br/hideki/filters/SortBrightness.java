package br.hideki.filters;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Comparator;
import java.util.Vector;

public class SortBrightness implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		int area = image.getWidth() * image.getHeight();
		for(int i = 0; i < area/8; i++ ) {
			int randX = (int)(Math.random()*(image.getWidth()));
			int randY = (int)(Math.random()*(image.getHeight()));
			Point pixel = new Point(randX,randY);
			pixel = fixPixelPosition(pixel, image);
			Vector<Point> pixels = getRasterSample(pixel, image);
			sortAndApplyPixels(pixels,image);
		}
		return image;
	}
	
	private void sortAndApplyPixels(Vector<Point> pixels, BufferedImage image) {
		Vector<Color> colors = new Vector<>();
		for (Point pixel : pixels) {
			colors.add(new Color(image.getRGB(pixel.x, pixel.y)));
		}
		colors.sort(new Comparator<Color>() {
			@Override
			public int compare(Color o1, Color o2) {
				float[] HSB1 = Color.RGBtoHSB(o1.getRed(), o1.getGreen(), o1.getBlue(), null);
				float[] HSB2 = Color.RGBtoHSB(o2.getRed(), o2.getGreen(), o2.getBlue(), null);
				if(HSB1[2] < HSB2[2]) { return -1; }
				else if(HSB1[2] == HSB2[2]) { return 0; }
				else { return 1; }
			}
		});
		for (int i = 0; i < colors.size(); i++) {
			Point pixel = pixels.get(i);
			image.setRGB(pixel.x, pixel.y, colors.get(i).getRGB());
		}
	}
	
	private Vector<Point> getRasterSample(Point pixel, BufferedImage image) {
		Vector<Point> rasterSample = new Vector<>();
		for (int j = 0; j < 15; j++) {
			Point pixelSample = pixel;
			if(pixelSample.x - j > image.getWidth()) { 
				pixelSample.setLocation(0, pixelSample.y + 1);
			}
			if(pixelSample.x - j < 0) {
				pixelSample.setLocation(14, pixelSample.y);
			}
			if(pixelSample.y > image.getHeight()) {
				pixelSample.setLocation(pixelSample.x, 0);
			}
			pixelSample.setLocation(pixelSample.x - j, pixelSample.y);
			rasterSample.add(pixelSample);
		}
		return rasterSample;
	}
	
	private Point fixPixelPosition(Point pixel, BufferedImage image) {
		if(pixel.x < 15 && pixel.y == 0) {
			if(pixel.getY() == 0) {
				pixel.setLocation(15, pixel.y);
			}else {
				pixel.setLocation(pixel.x, pixel.y - 1);
			}
		}
		return pixel;
	}
	
	@Override
	public String toString() {
		return "Sort Brightness";
	}

}
