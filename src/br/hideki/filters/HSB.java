package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class HSB implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				float[] hsb = new float[3];
				hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
				image.setRGB(i, j, new Color(Math.abs(255 - (int)(hsb[0]*255)),
											 Math.abs(255 - (int)(hsb[1]*255)),
											 Math.abs(255 - (int)(hsb[2]*255))).getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "HSB";
	}

}
