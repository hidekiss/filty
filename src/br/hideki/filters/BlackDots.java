package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class BlackDots implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		int area = image.getWidth() * image.getHeight();
		for(int i = 0; i < area/8; i++ ) {
			int randX = (int)(Math.random()*(image.getWidth()));
			int randY = (int)(Math.random()*(image.getHeight()));
			if(randX < 4 && randY == 0) {
				if(randY == 0) {
					randX = 4;
				}else {
					randY--;
				}
			}
			int weigth = 1;
			for (int j = 0; j < 4; j++) {
				if(randX - j > image.getWidth()) { 
					randX = 0;
					randY++;
				}
				if(randX - j < 0) {
					randX = 3;
				}
				if(image.getHeight() < randY) {
					randY = 0;
				}
				applyWeightedColorIn(image, new Color(image.getRGB(randX - j, randY)),weigth ,randX - j ,randY);
				weigth++;
			}
		}
		return image;
	}
	
	private void applyWeightedColorIn(BufferedImage image, Color color, int weigth , int posX, int posY) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int alpha = color.getAlpha();
		int max = Math.max(Math.max(red,green),blue);
		max = ((max *  weigth)/45);
		color = new Color((red + max)/2, (green + max)/2, (blue + max)/2, alpha);
		image.setRGB(posX, posY, color.getRGB());
	}
	
	@Override
	public String toString() {
		return "Black Dots";
	}
}
