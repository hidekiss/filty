package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class PixelSort implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		int area = image.getWidth() * image.getHeight();
		for(int i = 0; i < area/2; i++ ) {
			int randX = (int)(Math.random()*(image.getWidth()));
			int randY = (int)(Math.random()*(image.getHeight()));
			if(randX < 150 && randY == 0) {
				if(randY == 0) {
					randX = 150;
				}else {
					randY--;
				}
			}
			for (int j = 0; j < 150; j++) {
				if(randX - j > image.getWidth()) { 
					randX = 0;
					randY++;
				}
				if(randX - j < 0) {
					randX = 149;
				}
				if(image.getHeight() < randY) {
					randY = 0;
				}
				sortBubblyPixels(image,randX - j, randY,randX ,randY);
			}
		}
		return image;
	}
	private void sortBubblyPixels(BufferedImage image,int x1,int y1,int x2, int y2) {
		Color color1 = new Color(image.getRGB(x1, y1));
		Color color2 = new Color(image.getRGB(x2, y2));
		int red1 = color1.getRed();
		int red2 = color2.getRed();
		int green1 = color1.getGreen();
		int green2 = color2.getGreen();
		int blue1 = color1.getBlue();
		int blue2 = color2.getBlue();
		if(red1 > red2) {
			image.setRGB(x2, y2, new Color(red1,green1,blue1).getRGB());
			image.setRGB(x1, y1, new Color(red2,green2,blue2).getRGB());
		}else {
			image.setRGB(x1, y1, new Color(red1,green1,blue1).getRGB());
			image.setRGB(x2, y2, new Color(red2,green2,blue2).getRGB());
		}
	}
	
	@Override
	public String toString() {
		return "Pixel Sort";
	}

}
