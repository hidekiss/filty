package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Desaturate implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				int red = color.getRed();
				int green = color.getGreen();
				int blue = color.getBlue();
				int alpha = color.getAlpha();
				int max = Math.max(Math.max(red, green),blue);
				Color newColorSet = new Color((red+max)/2,(green+max)/2,(blue + max)/2,alpha);
				image.setRGB(i, j, newColorSet.getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "Desaturate";
	}

}
