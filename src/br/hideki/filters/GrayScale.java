package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class GrayScale implements Filterable{
		@Override
		public BufferedImage apply(BufferedImage image) {
			for (int x = 0; x < image.getWidth(); x++) {
				for (int y = 0; y < image.getHeight(); y++) {
					Color imageColors = new Color(image.getRGB(x, y));
					int red = imageColors.getRed(); 
					int green = imageColors.getGreen();
					int blue = imageColors.getBlue();
					int alpha = imageColors.getAlpha();	
					int grey = (red + green + blue)/3;
					Color newColorSet = new Color(grey,grey,grey,alpha);
					image.setRGB(x, y,newColorSet.getRGB());
				}
			}
			return image;
		}
		
		@Override
		public String toString() {
			return "Gray Scale";
		}
}
