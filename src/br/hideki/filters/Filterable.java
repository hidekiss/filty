package br.hideki.filters;

import java.awt.image.BufferedImage;

public interface Filterable {
	BufferedImage apply(BufferedImage image);
}
