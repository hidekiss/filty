package br.hideki.filters;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class TestKernel implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		Kernel kernel = new Kernel(3,3,new float[] {1.2f,1.2f,1.2f,
													-0.01f,-0.01f,-0.01f,
													-0.9f,-0.9f,-0.9f});
		ConvolveOp gausianBlur = new ConvolveOp(kernel);
		image = gausianBlur.filter(image, null);
		return image;
	}
	
	@Override
	public String toString() {
		return "* T e s t Kernel*";
	}
}
