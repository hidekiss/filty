package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Sharpen3 implements Filterable {
	
	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 1; i < image.getWidth(); i++) {
			for (int j = 1; j < image.getHeight(); j++) {
				Color prevColor = new Color(image.getRGB(i-1, j-1));
				Color currentColor = new Color(image.getRGB(i, j));
				int red = (int)(currentColor.getRed() + 0.5*(currentColor.getRed() - prevColor.getRed()));
				int green = (int)(currentColor.getGreen() + 0.5*(currentColor.getGreen() - prevColor.getBlue()));
				int blue = (int)(currentColor.getBlue() + 0.5*(currentColor.getBlue() - prevColor.getBlue()));
				int alpha = currentColor.getAlpha();
				Color newColorSet = fixColors(red, green, blue, alpha);
				image.setRGB(i, j, newColorSet.getRGB());
			}
		}
		return image;
	}
	
	private Color fixColors(int red, int green,int blue, int alpha) {
			if(red > 255) {
				red = 255;
			}
			if(red < 0) {
				red = 0;
			}
	
			if(green > 255) {
				green = 255;
			}
			if(green < 0){
				green = 0;
			}
		
			if(blue > 255) {
				blue = 255;
			}
			if(blue < 0) {
				blue = 0;
			}
		return new Color(red, green, blue, alpha);
	}
	
	@Override
	public String toString() {
		return "Sharpen #3";
	}

}
