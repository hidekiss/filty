package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class SortBGR implements Filterable{
	
	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				int[] rgb = {color.getRed(),color.getGreen(),color.getBlue()};
				Arrays.sort(rgb);
				image.setRGB(i, j, new Color(rgb[2],rgb[1],rgb[0]).getRGB());
			}
		}
		return image;
	}
		
	@Override
	public String toString() {
		return "Sort BGR";
	}
}
