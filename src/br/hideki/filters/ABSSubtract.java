package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ABSSubtract implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth()-1; i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color1 = new Color(image.getRGB(i, j));
				Color color2 = new Color(image.getRGB(i+1, j));
				int red1 = color1.getRed();
				int green1 = color1.getGreen();
				int blue1 = color1.getBlue();
				int alpha = color1.getAlpha();
				int red2 = color2.getRed();
				int green2 = color2.getGreen();
				int blue2 = color2.getBlue();
				image.setRGB(i, j, new Color(Math.abs(red2-red1),Math.abs(green2-green1),Math.abs(blue2-blue1),alpha).getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "ABS Subtraction";
	}

}
