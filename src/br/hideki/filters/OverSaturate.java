package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class OverSaturate implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				int red = color.getRed();
				int green = color.getGreen();
				int blue = color.getBlue();
				int alpha = color.getAlpha();
				
				if(red > 127) { red = 255; } 
				else { red = 0; }
				
				if(green > 127) { green = 255; }
				else { green = 0; }
				
				if(blue > 127) { blue = 255; }
				else { blue = 0; }
				
				image.setRGB(i, j, new Color(red, green, blue, alpha).getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "Over Saturate";
	}


}
