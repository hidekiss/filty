	package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Test implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth()-1; i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color curretColor = new Color(image.getRGB(i, j));
				Color nextColor = new Color (image.getRGB(i+1, j));
				float[] currentHSB = Color.RGBtoHSB(curretColor.getRed(), 
												    curretColor.getGreen(), 
												    curretColor.getBlue(), null);
				float[] nextHSB = Color.RGBtoHSB(nextColor.getRed(), 
												 nextColor.getGreen(), 
												 nextColor.getBlue(), null);
				if(currentHSB[1] > nextHSB[1]) {
					int rgbCurrent = Color.HSBtoRGB(currentHSB[0], currentHSB[1], currentHSB[2]);
					image.setRGB(i+1, j, rgbCurrent);
					int rgbNext = Color.HSBtoRGB(nextHSB[0], nextHSB[1], nextHSB[2]);
					image.setRGB(i, j, rgbNext);
				}
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "* T e s t *";
	}
}