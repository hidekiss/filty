package br.hideki.filters;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class Sharpen implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		Kernel kernel = new Kernel(3,3,new float[] {-1f,-1f,-1f,
													-1f,9f,-1f,
													-1f,-1f,-1f});
		ConvolveOp gausianBlur = new ConvolveOp(kernel);
		image = gausianBlur.filter(image, null);
		return image;
	}
	
	@Override
	public String toString() {
		return "Sharpen #0";
	}

}
