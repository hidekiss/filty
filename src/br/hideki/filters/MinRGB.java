package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class MinRGB implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				int red = color.getGreen();
				int green = color.getBlue();
				int blue = color.getRed();
				int alpha = color.getAlpha();
				int min = Math.min(Math.min(red, green), blue);
				if(min == red) {
					red = 0;
				}else if(min == green) {
					green = 0;
				}else {
					blue = 0;
				}
				Color newColorSet = new Color(red,green,blue,alpha);
				image.setRGB(i, j, newColorSet.getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "Minimize Min Color";
	}
}
