package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class InvertedNoise implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		int area = image.getWidth() * image.getHeight();
		for(int i = 0; i < area/32; i++ ) {
			int randX = (int)(Math.random()*(image.getWidth()));
			int randY = (int)(Math.random()*(image.getHeight()));
			if(randX < 15 && randY == 0) {
				if(randY == 0) {
					randX = 15;
				}else {
					randY--;
				}
			}
			for (int j = 0; j < 15; j++) {
				if(randX - j > image.getWidth()) { 
					randX = 0;
					randY++;
				}
				if(randX - j < 0) {
					randX = 14;
				}
				if(image.getHeight() < randY) {
					randY = 0;
				}
				applyColorIn(image, new Color(image.getRGB(randX - j, randY)) ,randX - j ,randY);
			}
		}
		return image;
	}
	
	private void applyColorIn(BufferedImage image, Color color, int posX, int posY) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int alpha = color.getAlpha();
		color = new Color((255 - red), (255 - green), (255 - blue), alpha);
		image.setRGB(posX, posY, color.getRGB());
	}
	
	@Override
	public String toString() {
		return "Inverted Noise";
	}
}
