package br.hideki.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class Pink implements Filterable {

	@Override
	public BufferedImage apply(BufferedImage image) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Color color = new Color(image.getRGB(i, j));
				int red = 255;
				int green = (color.getGreen() + red)/3;
				int blue = (255+red)/3;
				int alpha = color.getAlpha();
				Color newColorSet = new Color(red,green,blue, alpha);
				image.setRGB(i, j, newColorSet.getRGB());
			}
		}
		return image;
	}
	
	@Override
	public String toString() {
		return "Pink";
	}

}
